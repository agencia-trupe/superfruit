exports.development = {
    vhost  : 'superfruit.dev',
    stylus : './resources/assets/stylus/',
    js     : './resources/assets/js/',
    img    : './resources/assets/img/',
    vendor : '../public/assets/vendor/'
};

exports.build = {
    css : '../public/assets/css/',
    js  : '../public/assets/js/',
    img : '../public/assets/img/layout/'
};

exports.vendorPlugins = [
    // this.development.vendor + '',
];
