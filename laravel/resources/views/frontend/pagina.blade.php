@extends('frontend.common.template')

@section('content')

    <div class="main main-{{ $categoria }} pagina">
        <div class="center">
            <div class="padded">
                <h1>{{ $pagina->titulo }}</h1>

                <div class="texto">
                    {!! $pagina->texto_1 !!}
                </div>
            </div>

            @if(count($pagina->imagens))
            <div class="imagens">
                <div class="imagens-wrapper">
                    @foreach($pagina->imagens as $imagem)
                    <img src="{{ asset('assets/img/paginas/'.$imagem->imagem) }}" alt="">
                    @endforeach
                </div>
            </div>
            @endif

            @if($pagina->texto_2)
            <div class="padded">
                <div class="texto">
                    {!! $pagina->texto_2 !!}
                </div>
            </div>
            @endif
        </div>
    </div>

@endsection