    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">Liotécnica Tecnologia em Alimentos</a></h1>
            <h2>{{ $categoria === 'acai' ? 'Açaí' : 'Acerola' }}</h2>

            <button id="mobile-toggle" class="{{ $categoria }}" type="button" role="button">
                <span class="lines"></span>
            </button>

            <nav class="nav-{{ $categoria }}" id="nav-menu">
                <a href="{{ route('home') }}">Home</a>
                @foreach($paginasMenu->where('categoria', $categoria) as $p)
                <a href="{{ route('pagina', [$p->categoria, $p->slug]) }}" class="{{ isset($pagina) && $pagina->slug === $p->slug ? 'active' : '' }}">{{ $p->menu }}</a>
                @endforeach
                <a href="{{ route('curiosidades', $categoria) }}" class="{{ str_is('curiosidades*', Route::currentRouteName()) ? 'active' : '' }}">Curiosities</a>
                <a href="{{ route('contato', $categoria) }}" class="{{ str_is('contato*', Route::currentRouteName()) ? 'active' : '' }}">Contact Us</a>
            </nav>
        </div>
    </header>
