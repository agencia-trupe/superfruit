    <footer>
        <div class="center">
            <div class="footer-col">
                <p class="titulo">Açaí</p>
                <ul>
                    @foreach($paginasMenu->where('categoria', 'acai') as $pagina)
                    <li><a href="{{ route('pagina', [$pagina->categoria, $pagina->slug]) }}">{{ $pagina->menu }}</a></li>
                    @endforeach
                    <li><a href="{{ route('curiosidades', 'acai') }}">Curiosities</a></li>
                    <li><a href="{{ route('contato', 'acai') }}">Contact Us</a></li>
                </ul>
            </div>

            <div class="footer-col">
                <p class="titulo">Acerola</p>
                <ul>
                    @foreach($paginasMenu->where('categoria', 'acerola') as $pagina)
                    <li><a href="{{ route('pagina', [$pagina->categoria, $pagina->slug]) }}">{{ $pagina->menu }}</a></li>
                    @endforeach
                    <li><a href="{{ route('curiosidades', 'acerola') }}">Curiosities</a></li>
                    <li><a href="{{ route('contato', 'acerola') }}">Contact Us</a></li>
                </ul>
            </div>

            <div class="footer-col">
                <p class="titulo titulo-logo"></p>
                <p>© Copyright {{ date('Y') }}<br>Liotécnica Food Ingredients</p>
                <p class="trupe">
                    <a href="http://www.trupe.net" target="_blank">Site</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
