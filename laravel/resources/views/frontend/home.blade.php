@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="center">
            <h1>Liotécnica Food Ingredients</h1>
            <h2>A Living Tradition of Excellence</h2>

            <div class="home-banner">
                <a href="{{ route('pagina', 'acai') }}" class="acai">
                    <div class="banner-inner">
                        <h2>Açaí</h2>
                        <p>Antioxidant power from<br> the Amazon</p>
                    </div>
                </a>
                <a href="{{ route('pagina', 'acerola') }}" class="acerola">
                    <div class="banner-inner">
                        <h2>Acerola</h2>
                        <p>Nature's abundant source of<br> real vitamin C</p>
                    </div>
                </a>
            </div>
        </div>
    </div>

@endsection
