@extends('frontend.common.template')

@section('content')

    <div class="main main-{{ $categoria }} curiosidades">
        <div class="center">
            <div class="padded">
                <h1>Curiosities</h1>
                <h1 class="curiosidade-titulo">{{ $curiosidade->titulo }}</h1>

                <div class="texto">
                    {!! $curiosidade->texto !!}
                </div>

                <a href="{{ route('curiosidades', $categoria) }}" class="back">back</a>
            </div>
        </div>
    </div>

@endsection