@extends('frontend.common.template')

@section('content')

    <div class="main main-{{ $categoria }} curiosidades">
        <div class="center">
            <div class="padded">
                <h1>Curiosities</h1>

                <div class="curiosidades-lista">
                    @foreach($curiosidades as $curiosidade)
                    <a href="{{ route('curiosidades', [$categoria, $curiosidade->slug]) }}">{{ $curiosidade->titulo }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection