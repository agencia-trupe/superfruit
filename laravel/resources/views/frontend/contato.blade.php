@extends('frontend.common.template')

@section('content')

    <div class="main main-{{ $categoria }} contato">
        <div class="center">
            <div class="padded">
                <h1>Contact us</h1>
                <p class="chamada">To learn more about Liotécnica products or to place an order, please contact us:</p>

                <div class="contato-info">
                    <img src="{{ asset('assets/img/layout/img-fachada.png') }}" alt="">
                    <div class="endereco">
                        {!! $contato->endereco !!}
                    </div>
                    @foreach(explode(',', $contato->telefones) as $telefone)
                        <p class="telefone">{{ trim($telefone) }}</p>
                    @endforeach
                </div>

                <form action="" id="form-contato" method="POST">
                    <input type="text" name="nome" id="nome" placeholder="name" required>
                    <input type="email" name="email" id="email" placeholder="e-mail" required>
                    <input type="text" name="telefone" id="telefone" placeholder="phone number">
                    <input type="text" name="pais" id="pais" placeholder="country" required>
                    <textarea name="mensagem" id="mensagem" placeholder="message" required></textarea>
                    <div id="form-contato-response"></div>
                    <input type="submit" value="SEND">
                </form>
            </div>
        </div>
    </div>

@endsection