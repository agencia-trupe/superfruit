@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.paginas.index', $pagina->categoria) }}" title="Voltar para {{ $categorias[$pagina->categoria] }}" class="btn btn-default">&larr; Voltar para {{ $categorias[$pagina->categoria] }}</a>

    <legend>
        <h2>
            <small>Páginas / {{ $categorias[$pagina->categoria] }} / {{ $pagina->titulo }} /</small> Imagens
            @unless(count($registros) >= 3)
            <a href="{{ route('painel.paginas.imagens.create', $pagina) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Imagem</a>
            @endif
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="imagens">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>imagem</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ asset('assets/img/paginas/'.$registro->imagem) }}" style="height: 150px" alt=""></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.paginas.imagens.destroy', $pagina, $registro],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
