@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Páginas / {{ $categorias[$pagina->categoria] }} / {{ $pagina->titulo }} /</small> Adicionar Imagem</h2>
    </legend>

    {!! Form::open(['route' => ['painel.paginas.imagens.store', $pagina], 'files' => true]) !!}

        @include('painel.imagens.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
