@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2><small>Páginas /</small> {{ $categorias[$categoria] }}</h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info">
        <thead>
            <tr>
                <th>Título</th>
                <th>Imagens</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
                <td><a href="{{ route('painel.paginas.imagens.index', $registro) }}" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a></td>
                <td class="crud-actions">
                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.paginas.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
