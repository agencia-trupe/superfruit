@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_1', 'Texto 1') !!}
    {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'paginas']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_2', 'Texto 2') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'paginas']) !!}
</div>

{!! Form::hidden('categoria', (isset($registro) ? $registro->categoria : $categoria)) !!}
{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.paginas.index', (isset($registro) ? $registro->categoria : $categoria)) }}" class="btn btn-default btn-voltar">Voltar</a>
