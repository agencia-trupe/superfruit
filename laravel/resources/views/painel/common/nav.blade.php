<ul class="nav navbar-nav">
	   <li class="dropdown @if(str_is('painel.paginas*', Route::currentRouteName())) active @endif">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Páginas
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('painel.paginas.index', 'acai') }}">Açaí</a></li>
                <li><a href="{{ route('painel.paginas.index', 'acerola') }}">Acerola</a></li>
            </ul>
        </li>
	<li class="dropdown @if(str_is('painel.curiosidades*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Curiosidades
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.curiosidades.index', 'acai') }}">Açaí</a></li>
            <li><a href="{{ route('painel.curiosidades.index', 'acerola') }}">Acerola</a></li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
