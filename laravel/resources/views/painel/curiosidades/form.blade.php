@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'curiosidades']) !!}
</div>

{!! Form::hidden('categoria', (isset($registro) ? $registro->categoria : $categoria)) !!}
{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.curiosidades.index', (isset($registro) ? $registro->categoria : $categoria)) }}" class="btn btn-default btn-voltar">Voltar</a>
