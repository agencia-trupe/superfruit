@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Curiosidades / {{ $categorias[$categoria] }} /</small> Adicionar Curiosidade</h2>
    </legend>

    {!! Form::open(['route' => 'painel.curiosidades.store', 'files' => true]) !!}

        @include('painel.curiosidades.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
