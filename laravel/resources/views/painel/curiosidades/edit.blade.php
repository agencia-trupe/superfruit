@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Curiosidades / {{ $categorias[$registro->categoria] }} /</small> Editar Curiosidade</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.curiosidades.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.curiosidades.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
