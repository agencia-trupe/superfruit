@extends('frontend.common.template')

@section('content')

    <div class="home not-found">
        <div class="center">
            <h1>Liotécnica Food Ingredients</h1>
            <h2>A Living Tradition of Excellence</h2>

            <p>Página não encontrada</p>
            <a href="{{ route('home') }}">Voltar para a página inicial</a>
        </div>
    </div>

@endsection
