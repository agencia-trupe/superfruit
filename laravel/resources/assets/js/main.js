(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('nav');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.bgHeight = function() {
        var $bg = $('.fixed-bg');
        if (! $bg.length) return;

        $(window).resize(function() {
            if ($(window).width() > 1080) {
                var footerHeight = $('footer').outerHeight();
                var footerDistanceFromTop = $('footer').offset().top + footerHeight;
                var viewportHeight = $(window).innerHeight();

                var bgHeight = footerDistanceFromTop < viewportHeight ? (footerDistanceFromTop - footerHeight) : '100%';

                $bg.css('height', bgHeight);
            } else {
                $bg.css('height', '100%');
            }
        });

        $(window).trigger('resize');
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contact',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                pais: $('#pais').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Please fill in all required fields.').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bgHeight();
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
