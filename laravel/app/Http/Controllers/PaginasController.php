<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Pagina;

class PaginasController extends Controller
{
    private $categorias = ['acai', 'acerola'];

    public function index($categoria, $pagina = null) {
        if (! in_array($categoria, $this->categorias)) return abort('404');

        if (! $pagina) {
            $pagina = Pagina::with('imagens')->where('categoria', $categoria)->first();
        } else {
            $pagina = Pagina::with('imagens')->where('categoria', $categoria)->where('slug', $pagina)->first();
        }

        if (! $pagina) return abort('404');

        view()->share('categoria', $categoria);
        return view('frontend.pagina', compact('pagina'));
    }
}
