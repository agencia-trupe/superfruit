<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Curiosidade;

class CuriosidadesController extends Controller
{
    private $categorias = ['acai', 'acerola'];

    public function index($categoria, $slug = null) {
        if (! in_array($categoria, $this->categorias)) return abort('404');

        view()->share('categoria', $categoria);

        if (! $slug) {
            $curiosidades = Curiosidade::where('categoria', $categoria)->ordenados()->get();

            return view('frontend.curiosidades.index', compact('curiosidades'));
        }

        $curiosidade = Curiosidade::where('categoria', $categoria)->where('slug', $slug)->first();
        if (! $curiosidade) return abort('404');

        return view('frontend.curiosidades.show', compact('curiosidade'));
    }
}
