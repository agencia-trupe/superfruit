<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;

use App\Models\Contato;
use App\Models\ContatoRecebido;

class ContatoController extends Controller
{
    private $categorias = ['acai', 'acerola'];

    public function index($categoria) {
        if (! in_array($categoria, $this->categorias)) return abort('404');

        $contato = Contato::first();

        view()->share('categoria', $categoria);
        return view('frontend.contato', compact('contato'));
    }

    public function post(ContatosRecebidosRequest $request)
    {
        ContatoRecebido::create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Message sent successfully!'
        ];

        return response()->json($response);
    }
}
