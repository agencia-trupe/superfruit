<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\Pagina;
use App\Models\Imagem;

use App\Helpers\CropImage;

class ImagensController extends Controller
{
    private $categorias = [
        'acai'    => 'Açaí',
        'acerola' => 'Acerola'
    ];

    private $image_config = [
        'width'  => null,
        'height' => 200,
        'path'   => 'assets/img/paginas/'
    ];

    public function index(Pagina $pagina)
    {
        $registros = Imagem::where('pagina_id', $pagina->id)->ordenados()->get();

        return view('painel.imagens.index', compact('registros', 'pagina'))
            ->with(['categorias' => $this->categorias ]);
    }

    public function create(Pagina $pagina)
    {
        return view('painel.imagens.create', compact('pagina'))
            ->with(['categorias' => $this->categorias ]);
    }

    public function store(Pagina $pagina, ImagensRequest $request)
    {
        if (Imagem::where('pagina_id', $pagina->id)->count() >= 3) {
            return redirect()->route('painel.paginas.imagens.index', $pagina)->withErrors([
                'Erro ao adicionar imagem: limite de imagens excedido.'
            ]);
        }

        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            $pagina->imagens()->create($input);
            return redirect()->route('painel.paginas.imagens.index', $pagina)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Pagina $pagina, Imagem $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.paginas.imagens.index', $pagina)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
