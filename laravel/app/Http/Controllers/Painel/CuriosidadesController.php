<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CuriosidadesRequest;
use App\Http\Controllers\Controller;

use App\Models\Curiosidade;

class CuriosidadesController extends Controller
{
    private $categorias = [
        'acai'    => 'Açaí',
        'acerola' => 'Acerola'
    ];

    public function index($categoria = null)
    {
        if (! $categoria || ! array_key_exists($categoria, $this->categorias)) abort('404');

        $registros = Curiosidade::where('categoria', $categoria)->ordenados()->get();

        return view('painel.curiosidades.index', compact('registros', 'categoria'))
            ->with(['categorias' => $this->categorias ]);
    }

    public function create($categoria = null)
    {
        if (! $categoria || ! array_key_exists($categoria, $this->categorias)) abort('404');

        return view('painel.curiosidades.create', compact('categoria'))
            ->with(['categorias' => $this->categorias ]);
    }

    public function store(CuriosidadesRequest $request)
    {
        try {

            $input = $request->all();

            Curiosidade::create($input);
            return redirect()->route('painel.curiosidades.index', $input['categoria'])->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Curiosidade $registro)
    {
        return view('painel.curiosidades.edit', compact('registro'))
            ->with(['categorias' => $this->categorias ]);
    }

    public function update(CuriosidadesRequest $request, Curiosidade $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);
            return redirect()->route('painel.curiosidades.index', $input['categoria'])->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Curiosidade $registro)
    {
        try {

            $categoria = $registro->categoria;
            $registro->delete();
            return redirect()->route('painel.curiosidades.index', $categoria)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
