<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PaginasRequest;
use App\Http\Controllers\Controller;

use App\Models\Pagina;

class PaginasController extends Controller
{
    private $categorias = [
        'acai'    => 'Açaí',
        'acerola' => 'Acerola'
    ];

    public function index($categoria = null)
    {
        if (! $categoria || ! array_key_exists($categoria, $this->categorias)) abort('404');

        $registros = Pagina::where('categoria', $categoria)->get();

        return view('painel.paginas.index', compact('registros', 'categoria'))
            ->with(['categorias' => $this->categorias ]);
    }

    public function edit(Pagina $registro)
    {
        return view('painel.paginas.edit', compact('registro'))
            ->with(['categorias' => $this->categorias ]);
    }

    public function update(PaginasRequest $request, Pagina $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);
            return redirect()->route('painel.paginas.index', $input['categoria'])->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }
}
