<?php

Route::group(['middleware' => ['web']], function () {
    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        Route::get('curiosidades/{categoria?}', 'CuriosidadesController@index')
            ->name('painel.curiosidades.index');
        Route::get('curiosidades/{categoria?}/create', 'CuriosidadesController@create')
            ->name('painel.curiosidades.create');
		Route::resource('curiosidades', 'CuriosidadesController', [
            'except' => ['index', 'create']
        ]);

        Route::get('paginas/{categoria?}', 'PaginasController@index')
            ->name('painel.paginas.index');
		Route::resource('paginas', 'PaginasController', [
            'except' => ['index', 'create', 'store', 'destroy']
        ]);
		Route::resource('paginas.imagens', 'ImagensController');

        /* GENERATED ROUTES */
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });

    // Front-end
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('contact', 'ContatoController@post')->name('contato.post');
    Route::get('{categoria}/curiosities/{slug?}', 'CuriosidadesController@index')
        ->name('curiosidades');
    Route::get('{categoria}/contact', 'ContatoController@index')->name('contato');
    Route::get('{categoria}/{pagina_slug?}', 'PaginasController@index')->name('pagina');
});
