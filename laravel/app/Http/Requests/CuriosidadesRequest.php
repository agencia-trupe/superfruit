<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CuriosidadesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'categoria' => 'required',
            'titulo' => 'required',
            'slug' => '',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
