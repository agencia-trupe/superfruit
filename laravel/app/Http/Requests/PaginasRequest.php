<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PaginasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'texto_1' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
