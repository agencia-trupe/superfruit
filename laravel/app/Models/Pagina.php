<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pagina extends Model
{
    protected $table = 'paginas';

    protected $guarded = ['id'];

    public function imagens()
    {
        return $this->hasMany(Imagem::class, 'pagina_id')->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
