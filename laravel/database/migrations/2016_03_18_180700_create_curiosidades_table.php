<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuriosidadesTable extends Migration
{
    public function up()
    {
        Schema::create('curiosidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('categoria');
            $table->string('titulo');
            $table->string('slug');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('curiosidades');
    }
}
