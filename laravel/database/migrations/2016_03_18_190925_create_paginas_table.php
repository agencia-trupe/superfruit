<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaginasTable extends Migration
{
    public function up()
    {
        Schema::create('paginas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('categoria');
            $table->string('menu');
            $table->string('titulo');
            $table->string('slug');
            $table->text('texto_1');
            $table->text('texto_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('paginas');
    }
}
