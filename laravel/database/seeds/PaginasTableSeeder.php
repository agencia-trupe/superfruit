<?php

use Illuminate\Database\Seeder;

class PaginasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paginas')->insert([
            [
                'categoria' => 'acai',
                'menu'      => 'Açaí',
                'titulo'    => 'Açaí Powder',
                'slug'      => 'acai-powder',
                'texto_1'   => '',
            ],
            [
                'categoria' => 'acai',
                'menu'      => 'Our Tradition',
                'titulo'    => 'Our Tradition',
                'slug'      => 'our-tradition',
                'texto_1'   => '',
            ],
            [
                'categoria' => 'acai',
                'menu'      => 'Packaging',
                'titulo'    => 'Packaging Options',
                'slug'      => 'packaging-options',
                'texto_1'   => '',
            ],
            [
                'categoria' => 'acai',
                'menu'      => 'Our Difference',
                'titulo'    => 'Our Difference',
                'slug'      => 'our-difference',
                'texto_1'   => '',
            ],
            [
                'categoria' => 'acerola',
                'menu'      => 'Acerola',
                'titulo'    => 'Acerola Powder',
                'slug'      => 'acerola-powder',
                'texto_1'   => '',
            ],
            [
                'categoria' => 'acerola',
                'menu'      => 'Our Tradition',
                'titulo'    => 'Our Tradition',
                'slug'      => 'our-tradition',
                'texto_1'   => '',
            ],
            [
                'categoria' => 'acerola',
                'menu'      => 'Packaging',
                'titulo'    => 'Packaging Options',
                'slug'      => 'packaging-options',
                'texto_1'   => '',
            ],
            [
                'categoria' => 'acerola',
                'menu'      => 'Our Difference',
                'titulo'    => 'Our Difference',
                'slug'      => 'our-difference',
                'texto_1'   => '',
            ],
        ]);
    }
}
