<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'     => 'export@liotecnica.com.br',
            'endereco'  => '<p>Liotécnica Headquarters</p><p>Factories/Sales Office</p><p>Av. João Paulo I, 1098</p><p>Embu - SP - Brazil</p><p>06817-000</p>',
            'telefones' => '+1 305 4249951, +55 11 4785-2386',
        ]);
    }
}
