-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: superfruit
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `telefones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'export@liotecnica.com.br','<p>Liotécnica Headquarters</p><p>Factories/Sales Office</p><p>Av. João Paulo I, 1098</p><p>Embu - SP - Brazil</p><p>06817-000</p>','+1 305 4249951, +55 11 4785-2386',NULL,NULL);
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curiosidades`
--

DROP TABLE IF EXISTS `curiosidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `curiosidades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curiosidades`
--

LOCK TABLES `curiosidades` WRITE;
/*!40000 ALTER TABLE `curiosidades` DISABLE KEYS */;
INSERT INTO `curiosidades` VALUES (1,0,'acai','Curiosidade Açaí','curiosidade-acai','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quam leo, scelerisque sed ante suscipit, tristique tincidunt ex. Aenean quis justo mi. Nullam lacinia vulputate magna, ut mattis lorem lacinia ut. Phasellus blandit est id odio dictum tempus. Proin eleifend ante eget urna volutpat gravida. Etiam porta mauris a lacinia ullamcorper. Aliquam sit amet blandit nibh. Nulla cursus magna non mauris blandit volutpat. Fusce in purus mauris. Etiam scelerisque hendrerit metus a consectetur. Pellentesque lobortis eleifend nunc, quis sodales libero ornare tristique.</p>\r\n\r\n<p><img src=\"http://superfruit.dev/assets/img/curiosidades/img-marcacao-curiosities_20160323194226.png\" /></p>\r\n\r\n<p>Mauris vitae pretium sem, nec molestie felis. Vestibulum neque lacus, semper id tellus vitae, ornare lacinia velit. Nam fringilla orci non nulla dignissim rhoncus. Pellentesque quis velit a nulla eleifend ullamcorper. Cras nec massa tellus. Quisque purus sem, tempor vitae turpis ac, varius egestas odio. Ut consectetur tempus orci eget dignissim. Vivamus viverra mollis purus, vitae mollis velit imperdiet a. Mauris varius pellentesque iaculis. Suspendisse tempus quam in ligula finibus pretium. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam euismod erat at felis tempor blandit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque sapien diam, auctor quis dictum eget, molestie ac augue.</p>\r\n\r\n<p>Nam blandit maximus lectus vel dapibus. Suspendisse auctor sem ex. In bibendum neque vel enim hendrerit, quis sodales diam convallis. Aenean pellentesque orci sed urna pellentesque semper quis ut nisi. Nulla pulvinar varius blandit. Donec vel condimentum ante, vitae cursus urna. Cras id dapibus leo. Nullam lobortis lacus ac ex ornare, at scelerisque odio convallis. Curabitur malesuada, dolor vitae hendrerit semper, enim magna luctus mauris, vel pellentesque eros urna eu tortor.</p>\r\n','2016-03-23 19:42:36','2016-03-23 19:42:36'),(2,0,'acerola','Curiosidade Acerola','curiosidade-acerola','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum quam leo, scelerisque sed ante suscipit, tristique tincidunt ex. Aenean quis justo mi. Nullam lacinia vulputate magna, ut mattis lorem lacinia ut. Phasellus blandit est id odio dictum tempus. Proin eleifend ante eget urna volutpat gravida. Etiam porta mauris a lacinia ullamcorper. Aliquam sit amet blandit nibh. Nulla cursus magna non mauris blandit volutpat. Fusce in purus mauris. Etiam scelerisque hendrerit metus a consectetur. Pellentesque lobortis eleifend nunc, quis sodales libero ornare tristique.</p>\r\n\r\n<p><img src=\"http://superfruit.dev/assets/img/curiosidades/img-marcacao-curiosities_20160323194226.png\" /></p>\r\n\r\n<p>Mauris vitae pretium sem, nec molestie felis. Vestibulum neque lacus, semper id tellus vitae, ornare lacinia velit. Nam fringilla orci non nulla dignissim rhoncus. Pellentesque quis velit a nulla eleifend ullamcorper. Cras nec massa tellus. Quisque purus sem, tempor vitae turpis ac, varius egestas odio. Ut consectetur tempus orci eget dignissim. Vivamus viverra mollis purus, vitae mollis velit imperdiet a. Mauris varius pellentesque iaculis. Suspendisse tempus quam in ligula finibus pretium. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam euismod erat at felis tempor blandit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Quisque sapien diam, auctor quis dictum eget, molestie ac augue.</p>\r\n\r\n<p>Nam blandit maximus lectus vel dapibus. Suspendisse auctor sem ex. In bibendum neque vel enim hendrerit, quis sodales diam convallis. Aenean pellentesque orci sed urna pellentesque semper quis ut nisi. Nulla pulvinar varius blandit. Donec vel condimentum ante, vitae cursus urna. Cras id dapibus leo. Nullam lobortis lacus ac ex ornare, at scelerisque odio convallis. Curabitur malesuada, dolor vitae hendrerit semper, enim magna luctus mauris, vel pellentesque eros urna eu tortor.</p>\r\n','2016-03-23 19:42:53','2016-03-23 19:42:53');
/*!40000 ALTER TABLE `curiosidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagens`
--

DROP TABLE IF EXISTS `imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagina_id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagens`
--

LOCK TABLES `imagens` WRITE;
/*!40000 ALTER TABLE `imagens` DISABLE KEYS */;
INSERT INTO `imagens` VALUES (1,1,0,'img-marcacao4_20160323194500.png','2016-03-23 19:45:00','2016-03-23 19:45:00'),(2,1,1,'img-marcacao5_20160323194505.png','2016-03-23 19:45:05','2016-03-23 19:45:05'),(3,1,2,'img-marcacao6_20160323194510.png','2016-03-23 19:45:10','2016-03-23 19:45:10'),(4,5,0,'img-marcacao3_20160323194540.png','2016-03-23 19:45:40','2016-03-23 19:45:40'),(5,5,0,'img-marcacao2_20160323194552.png','2016-03-23 19:45:52','2016-03-23 19:45:52'),(6,5,0,'img-marcacao1_20160323194557.png','2016-03-23 19:45:57','2016-03-23 19:45:57');
/*!40000 ALTER TABLE `imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_03_18_180700_create_curiosidades_table',1),('2016_03_18_190925_create_paginas_table',1),('2016_03_18_195827_create_imagens_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paginas`
--

DROP TABLE IF EXISTS `paginas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paginas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `menu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paginas`
--

LOCK TABLES `paginas` WRITE;
/*!40000 ALTER TABLE `paginas` DISABLE KEYS */;
INSERT INTO `paginas` VALUES (1,'acai','Açaí','Açaí Powder','acai-powder','<h2 class=\"subtitulo-1\">Lorem ipsum dolor sit amet, consectetur</h2>\r\n\r\n<p>Vestibulum quam leo, scelerisque sed ante suscipit, tristique tincidunt ex. Aenean quis justo mi. Nullam lacinia vulputate magna, ut mattis lorem lacinia ut. Phasellus blandit est id odio dictum tempus. Proin eleifend ante eget urna volutpat gravida. Etiam porta mauris a lacinia ullamcorper. Aliquam sit amet blandit nibh. Nulla cursus magna non mauris blandit volutpat. Fusce in purus mauris. Etiam scelerisque hendrerit metus a consectetur. Pellentesque lobortis eleifend nunc, quis sodales libero ornare tristique.</p>\r\n','<h2 class=\"subtitulo-2\">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>\r\n\r\n<p>Vestibulum quam leo, scelerisque sed ante suscipit, tristique tincidunt ex. Aenean quis justo mi. Nullam lacinia vulputate magna, ut mattis lorem lacinia ut. Phasellus blandit est id odio dictum tempus. Proin eleifend ante eget urna volutpat gravida. Etiam porta mauris a lacinia ullamcorper. Aliquam sit amet blandit nibh. Nulla cursus magna non mauris blandit volutpat. Fusce in purus mauris. Etiam scelerisque hendrerit metus a consectetur. Pellentesque lobortis eleifend nunc, quis sodales libero ornare tristique.</p>\r\n\r\n<ul>\r\n	<li>Mauris vitae pretium sem, nec molestie felis.</li>\r\n	<li>Vestibulum neque lacus, semper id tellus vitae, ornare lacinia velit.</li>\r\n	<li>Nam fringilla orci non nulla dignissim rhoncus.</li>\r\n	<li>Pellentesque quis velit a nulla eleifend ullamcorper. Cras nec massa tellus.</li>\r\n	<li>Quisque purus sem, tempor vitae turpis ac, varius egestas odio.</li>\r\n</ul>\r\n',NULL,'2016-03-23 19:47:32'),(2,'acai','Our Tradition','Our Tradition','our-tradition','','',NULL,NULL),(3,'acai','Packaging','Packaging Options','packaging-options','','',NULL,NULL),(4,'acai','Our Difference','Our Difference','our-difference','','',NULL,NULL),(5,'acerola','Acerola','Acerola Powder','acerola-powder','<h2 class=\"subtitulo-1\">Lorem ipsum dolor sit amet, consectetur</h2>\r\n\r\n<p>Vestibulum quam leo, scelerisque sed ante suscipit, tristique tincidunt ex. Aenean quis justo mi. Nullam lacinia vulputate magna, ut mattis lorem lacinia ut. Phasellus blandit est id odio dictum tempus. Proin eleifend ante eget urna volutpat gravida. Etiam porta mauris a lacinia ullamcorper. Aliquam sit amet blandit nibh. Nulla cursus magna non mauris blandit volutpat. Fusce in purus mauris. Etiam scelerisque hendrerit metus a consectetur. Pellentesque lobortis eleifend nunc, quis sodales libero ornare tristique.</p>\r\n','<h2 class=\"subtitulo-2\">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h2>\r\n\r\n<p>Vestibulum quam leo, scelerisque sed ante suscipit, tristique tincidunt ex. Aenean quis justo mi. Nullam lacinia vulputate magna, ut mattis lorem lacinia ut. Phasellus blandit est id odio dictum tempus. Proin eleifend ante eget urna volutpat gravida. Etiam porta mauris a lacinia ullamcorper. Aliquam sit amet blandit nibh. Nulla cursus magna non mauris blandit volutpat. Fusce in purus mauris. Etiam scelerisque hendrerit metus a consectetur. Pellentesque lobortis eleifend nunc, quis sodales libero ornare tristique.</p>\r\n\r\n<ul>\r\n	<li>Mauris vitae pretium sem, nec molestie felis.</li>\r\n	<li>Vestibulum neque lacus, semper id tellus vitae, ornare lacinia velit.</li>\r\n	<li>Nam fringilla orci non nulla dignissim rhoncus.</li>\r\n	<li>Pellentesque quis velit a nulla eleifend ullamcorper. Cras nec massa tellus.</li>\r\n	<li>Quisque purus sem, tempor vitae turpis ac, varius egestas odio.</li>\r\n</ul>\r\n',NULL,'2016-03-23 19:47:26'),(6,'acerola','Our Tradition','Our Tradition','our-tradition','','',NULL,NULL),(7,'acerola','Packaging','Packaging Options','packaging-options','','',NULL,NULL),(8,'acerola','Our Difference','Our Difference','our-difference','','',NULL,NULL);
/*!40000 ALTER TABLE `paginas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$/wLIjW4FTg5owM4xEcatoewxZ7fBaW0zoEy65DLAguO/nk241bHTK','1vxhY0rsTEwEiFrYfEvTv81Lto9R8vLkXeQ3faUu26BlEzlH1XkZm8SDPV4U',NULL,'2016-03-23 19:40:01');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-23 19:50:44
